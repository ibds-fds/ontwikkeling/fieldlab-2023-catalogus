# Fieldlab-2023-catalogus

Documenatatie: https://fds-fieldlab.apps.digilab.network/

## Fieldlab catalogus report
Voor de fieldlab demo API's een beschrijving in DCAT formaat met een verwijzing naar trefwoorden.


## Grafen
* Gemeente Woudendijk digi:graaf-gemeente-woudendijk
* Gemeente Zonnendaal digi:graaf-gemeente-zonnendaal
* Belastingsamenwerking Zonnenwoud digi:graaf-bsamenwerking-zonnenwoud
* Fictief Kadaster digi:graaf-kadaster
* Rijksdienst Realistische Demo's digi:rd-realistische-demo's
* Fieldlab Software digi:field-lab

Alleen de eerste drie zijn uitgewerkt.

## Samenstelling catalogus
Elke aparte graaf bevat een fragment van de trefwoorden catalogus zoals die voor de participant relevant is.

Bijvoorbeeld voor Zonnedaal:

```
ddc:top rdf:type skos:Concept ;
    skos:prefLabel "Top data concept";
    skos:narrower ddc:provincie-klein-nederland;
.

ddc:provincie-klein-nederland skos:prefLabel "Provincie Klein Nederland";
  rdf:type skos:Concept;
.


ddc:provincie-klein-nederland skos:narrower ddc:zonnedaal .

ddc:zonnedaal skos:prefLabel "Gemeente Zonnedaal" ;
  rdf:type skos:Concept;
.
```

Idem voor woudendijk.

Voor de samenwerking:

```
ddc:top rdf:type skos:Concept ;
    skos:narrower ddc:belasting;
    skos:prefLabel "Belasting"
.

ddc:belasting rdf:type skos:Concept ;
    skos:narrower ddc:WOZ-belasting;
    skos:prefLabel "WOZ-Belasting"
.

ddc:WOZ-belasting rdf:type skos:Concept ;
    skos:narrower ddc:WOZ-belasting-aanslag;
    skos:prefLabel "Belasting aanslag"
.
```

Daarmee onstaat de hierarchied per graaf voor een fragment, maar voor de default graaf voor alles als geheel:

```
      [-] > FIELDLAB
       |   [-] https://digilab.overheid.nl/projecten#graaf-gemeente-woudendijk
       |    |   [+] instances
       |    |   [-] concepts
       |    |    |   [+] Top data concept
       |    |    |   [-] https://digilab.overheid.nl/data-concept/def#provincie-klein-nederland
       |    |    |    |   [+] Gemeente Woudendijk
       |    |   [+] classes
       |   [+] https://digilab.overheid.nl/projecten#graaf-bsamenwerking-zonnewoud
       |   [-] https://digilab.overheid.nl/projecten#graaf-gemeente-zonnedaal
       |    |   [-] instances
       |    |    |   [+] http://www.w3.org/ns/dcat#Catalog
       |    |    |   [+] http://www.w3.org/ns/dcat#DataService
       |    |   [-] concepts
       |    |    |   [+] Top data concept
       |    |    |   [-] Provincie Klein Nederland
       |    |    |    |   [+] Gemeente Zonnedaal
       |    |    |   [+] Gemeente Zonnedaal
       |    |   [+] classes
       |   [-] DEFAULT
       |        [+] instances
       |        [-] concepts
       |         |   [+] Top data concept
       |         |   [+] Belasting
       |         |   [-] Provincie Klein Nederland
       |         |    |   [+] Gemeente Woudendijk
       |         |    |   [+] Gemeente Zonnedaal
       |         |   [+] Gemeente Woudendijk
       |         |   [+] WOZ-Belasting
       |         |   [+] Belasting aanslag
       |         |   [+] Gemeente Zonnedaal
```
Het gaat er om dat beide gemeentes vanzelf onder de provincie te voorschijn komen, omdat de grafen gemerged worden in de defaulst graaf.

### Catalogus trefwoorden koppelen aan een service
Dat gaat als volgt, in zonnewoud.trig:
```
    dct:subject ddc:WOZ-belasting;
```


